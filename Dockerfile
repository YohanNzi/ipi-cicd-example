# Image de base
FROM node:20-alpine

# Répertoire où seront copiés les fichiers,
# et d'où sera lancée l'application.
WORKDIR /app

# Copie du package.json et package-lock.json.
# Ces fichiers changeant a priori moins souvent que les fichiers de l'app,
# on les copie dès maintenant, afin de bénéficier de la mise en cache des
# "layers" constituant l'image (s'ils ne sont pas modifiés, seul le dernier COPY
# devra être ré-effectué au prochain build).
COPY package.json yarn.lock ./

# Installation des dépendances
RUN yarn install

# Copie de l'app (ici un seul fichier).
COPY dist dist

# Commande lancée au démarrage du conteneur
CMD node dist/index.js
# Syntaxe alternative
# CMD ["node", "index.js"]